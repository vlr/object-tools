import { expect } from "chai";
import { nullObject, extend } from "../src";

describe("nullObject", function (): void {
  it("should not have 'constructor' field", function (): void {
    // arrange

    // act
    const result = nullObject();

    // assert
    expect(result.constructor === undefined).to.be.equal(true);
  });
});

describe("extend", function (): void {
  it("should extend onto null object", function (): void {
    // arrange

    // act
    const result = extend({ a: 1}, { b: 2});

    // assert
    expect(result.constructor === undefined).to.be.equal(true);
    expect(result.a).to.be.equal(1);
    expect(result.b).to.be.equal(2);
  });
});
