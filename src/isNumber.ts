export function isNumber(toCheck: any): toCheck is number {
  return typeof toCheck === "number";
}
