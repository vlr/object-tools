export function nullObject<T = any>(): T {
  return Object.create(null);
}

export function extend<T = any>(src: any, ...extend: any[]): T {
  return Object.assign(nullObject(), src, ...extend);
}
