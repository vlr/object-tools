export function isString(toCheck: any): toCheck is string {
  return typeof toCheck === "string";
}
