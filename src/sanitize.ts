import { nullObject } from "./nullObject";
import { isObject } from "./isObject";
import { forIn } from "./forIn";

export function sanitize(obj: any): any {
  if (Array.isArray(obj)) {
    return obj.map(item => sanitize(item));
  }

  if (isObject(obj)) {
    const result = nullObject();
    forIn(obj, (value, field) => result[field] = sanitize(value));
    return result;
  }

  return obj;
}

export function parse<T>(input: string): T {
  return sanitize(JSON.parse(input));
}
