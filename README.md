# object-tools-ts
Set of DRY tools to work with object. Typescript style.

# isString isObject isNumber and isFunction
There are similar functions in util package, but those are marked deprecated. For isString there are notes to use typeof x === 'string' instead, and for other function there are similar notes. This lib is created to encapsulate that recommended code

# forIn
Iterates over fields of the object.

```
let a = { b: '1' };

forIn(a, (item, field) => console.log(item, field)); // b 1
```

# nullObject
Produces object which has null prototype

# extend
Copies fields onto new null object

```
const result = extend({ a: 1}, { b: 2}); // { a: 1, b: 2}
```

# sanitize and parse
sanitize removes prototype from object and all its fields recursively